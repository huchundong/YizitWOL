﻿namespace WOL
{
    partial class Form1
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.cbIpList = new System.Windows.Forms.ComboBox();
            this.cbNameList = new System.Windows.Forms.ComboBox();
            this.cbMacList = new System.Windows.Forms.ComboBox();
            this.tbInfo = new System.Windows.Forms.TextBox();
            this.btnSend = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // cbIpList
            // 
            this.cbIpList.FormattingEnabled = true;
            this.cbIpList.Location = new System.Drawing.Point(12, 140);
            this.cbIpList.Name = "cbIpList";
            this.cbIpList.Size = new System.Drawing.Size(121, 20);
            this.cbIpList.TabIndex = 0;
            // 
            // cbNameList
            // 
            this.cbNameList.FormattingEnabled = true;
            this.cbNameList.Location = new System.Drawing.Point(13, 90);
            this.cbNameList.Name = "cbNameList";
            this.cbNameList.Size = new System.Drawing.Size(291, 20);
            this.cbNameList.TabIndex = 1;
            this.cbNameList.SelectedIndexChanged += new System.EventHandler(this.cbNameList_SelectedIndexChanged);
            // 
            // cbMacList
            // 
            this.cbMacList.FormattingEnabled = true;
            this.cbMacList.Location = new System.Drawing.Point(174, 140);
            this.cbMacList.Name = "cbMacList";
            this.cbMacList.Size = new System.Drawing.Size(130, 20);
            this.cbMacList.TabIndex = 2;
            // 
            // tbInfo
            // 
            this.tbInfo.Location = new System.Drawing.Point(13, 12);
            this.tbInfo.Multiline = true;
            this.tbInfo.Name = "tbInfo";
            this.tbInfo.Size = new System.Drawing.Size(291, 72);
            this.tbInfo.TabIndex = 3;
            // 
            // btnSend
            // 
            this.btnSend.Location = new System.Drawing.Point(65, 184);
            this.btnSend.Name = "btnSend";
            this.btnSend.Size = new System.Drawing.Size(174, 62);
            this.btnSend.TabIndex = 4;
            this.btnSend.Text = "发送魔法包";
            this.btnSend.UseVisualStyleBackColor = true;
            this.btnSend.Click += new System.EventHandler(this.btnSend_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(317, 300);
            this.Controls.Add(this.btnSend);
            this.Controls.Add(this.tbInfo);
            this.Controls.Add(this.cbMacList);
            this.Controls.Add(this.cbNameList);
            this.Controls.Add(this.cbIpList);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox cbIpList;
        private System.Windows.Forms.ComboBox cbNameList;
        private System.Windows.Forms.ComboBox cbMacList;
        private System.Windows.Forms.TextBox tbInfo;
        private System.Windows.Forms.Button btnSend;
    }
}

