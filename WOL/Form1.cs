﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Windows.Forms;
using System.Management;

namespace WOL
{
    public partial class Form1 : Form
    {
        public static string ip;
        public static string mac;
        public static string port = "9";
        public static string[] iplist;
        public static string[] maclist;
        public static string[] namelist;
        public static string[] config = new string[3];
        static string root_path = System.Environment.CurrentDirectory;
        public static string config_path = root_path + @"\config";
        public static bool readConfig_result = true;

        public Form1()
        {
            InitializeComponent();
            try
            {
                config = System.IO.File.ReadAllLines(config_path);
                mac = config[0];
                ip = config[1];
                port = config[2];
                tbInfo.Text += mac + "\r\n" + ip + "\r\n" + port + "\r\n";
            }
            catch
            {
                readConfig_result = false;
                MessageBox.Show("读取配置错误");
            }

            getMac();
            cbIpList.Items.AddRange(iplist);
            cbMacList.Items.AddRange(maclist);
            cbNameList.Items.AddRange(namelist);
        }

        private string getMac()
        {
            try
            {
                ManagementClass mc;
                string hostInfo = Dns.GetHostName();

                System.Net.IPAddress[] addressList = Dns.GetHostEntry(Dns.GetHostName()).AddressList;
                int ip_count = 0;
                int mac_count = 0;
                iplist = new string[addressList.Length / 2];
                for (int i = addressList.Length / 2; i < addressList.Length; i++)
                {
                    iplist[ip_count++] = addressList[i].ToString();
                }
                //mac地址  
                mc = new ManagementClass("Win32_NetworkAdapterConfiguration");
                ManagementObjectCollection moc = mc.GetInstances();
                maclist = new string[moc.Count / 6];
                namelist = new string[moc.Count / 6];
                foreach (ManagementObject mo in moc)
                {
                    if (mo["IPEnabled"].ToString() == "True")
                    {
                        maclist[mac_count] = mo["MacAddress"].ToString();
                        namelist[mac_count] = mo["Caption"].ToString();
                        mac_count++;
                    }
                }
                //输出  
                string outPutStr = "IP:{0},\n MAC地址:{1},\n 服务名:{2}";
                outPutStr = string.Format(outPutStr, ip[0], mac[0], namelist[0]);
                return outPutStr;
            }
            catch (Exception e)
            {
                return "error";
            }

        }

        public int SendPacket()
        {

            System.Net.Sockets.UdpClient m_Client = new System.Net.Sockets.UdpClient();
            byte[] msg = new Byte[102];
            for (int i = 0; i < 5; i++)
            {
                msg[i] = 0xff;
            }
            string mac1 = mac.Substring(0, 2);
            string mac2 = mac.Substring(3, 2);
            string mac3 = mac.Substring(6, 2);
            string mac4 = mac.Substring(9, 2);
            string mac5 = mac.Substring(12, 2);
            string mac6 = mac.Substring(15, 2);
            try
            {
                for (int k = 6; k <= 96; k += 6)
                {
                    int tmac1 = Convert.ToInt32(mac1, 16);
                    msg[k + 0] = (byte)tmac1;
                    int tmac2 = Convert.ToInt32(mac2, 16);
                    msg[k + 1] = (byte)tmac2;
                    int tmac3 = Convert.ToInt32(mac3, 16);
                    msg[k + 2] = (byte)tmac3;
                    int tmac4 = Convert.ToInt32(mac4, 16);
                    msg[k + 3] = (byte)tmac4;
                    int tmac5 = Convert.ToInt32(mac5, 16);
                    msg[k + 4] = (byte)tmac5;
                    int tmac6 = Convert.ToInt32(mac6, 16);
                    msg[k + 5] = (byte)tmac6;
                }
            }
            catch
            {
                return -1;
            }

            for (int i = 0; i < 3; i++)
            {
                try
                {
                    IPAddress ipaddress = new IPAddress(new byte[] { 192, 168, 1, 121 });
                    IPEndPoint ipEndPoint = new IPEndPoint(ipaddress, 9);

                    int ecode = m_Client.Send(msg, msg.Length, ipEndPoint);
                }
                catch
                {
                    return -2;
                }
            }
            MessageBox.Show("发送成功！");
            return 0;
        }

        private void cbNameList_SelectedIndexChanged(object sender, EventArgs e)
        {
            int index = cbNameList.SelectedIndex;
            cbIpList.SelectedIndex = index;
            cbMacList.SelectedIndex = index;
            ip = cbIpList.Items[index].ToString();
            mac = cbMacList.Items[index].ToString();
            string[] config_now = { mac, ip, port };
            System.IO.File.WriteAllLines(config_path, config_now);

        }

        private void btnSend_Click(object sender, EventArgs e)
        {
            SendPacket();
        }
    }
}
